angular.module("app-container", [])
.controller('mainCtrl', function($scope, $http) {
	$scope.helloWorld = function() {
		$http.get(`https://api.unsplash.com/search/photos?page=1&per_page=100?&query=${$scope.search}&client_id=79812888d3772512ecf89e9930213cf5a38fe9bf0b65fe446b409bd90c26d2ec`)
		.then(function(json) {
		 	$scope.results = json.data.results;
		 	$scope.loaded = true;
		}, function errorCallback(response) {
			document.getElementById('error').style.display = "flex";
		})
	};
	$scope.loaded = false;
})

.controller('categoryCtrl', function($scope) {
	$scope.getCategory = function() {
		console.log("helloooooo.........again");
	};
})

.controller('searchCtrl', function($scope) {
	$scope.expandImage = function() {
		let item = this.item;
		console.log(item);
		let fullImage = document.getElementById('full-image');
		let fullTitle = document.getElementById('full-title');
		let fullArtist = document.getElementById('full-artist');
		let fullUrls = document.getElementById('full-urls');
		let fullLikes = document.getElementById('full-likes');
		let artistImage = document.getElementById('artist-image');
		document.getElementById('fullscreen').style.display = 'flex';
		document.getElementById('overlay').style.display = 'flex';
		document.getElementById('thumb').innerHTML = `${item.urls.thumb}`;
		document.getElementById('small').innerHTML = `${item.urls.small}`;
		document.getElementById('large').innerHTML = `${item.urls.full}`;
		fullImage.setAttribute("src",`${item.urls.raw}`);
		fullTitle.innerHTML = `${item.description}`;
		fullArtist.innerHTML = `${item.user.name}`;
		fullLikes.innerHTML = `${item.likes}`;
		artistImage.setAttribute("src",`${item.user.profile_image.small}`);
		console.log(item.description);
		console.log(fullTitle);
	};
})

.controller('hideCtrl', function($scope) {
	$scope.closeImage = function() {
		document.getElementById('overlay').style.display = "none";
		document.getElementById('fullscreen').style.display = "none";
		document.getElementById('small').style.display = "none";
		document.getElementById('large').style.display = "none";
		document.getElementById('thumb').style.display = "none";
	};

	$scope.getLinksSmall = function($scope) {
		document.getElementById('small').style.display = "flex";
	};
	$scope.getLinksLarge = function($scope) {
		document.getElementById('large').style.display = "flex";
	};
	$scope.getLinksThumb = function($scope) {
		document.getElementById('thumb').style.display = "flex";
	};
})


.controller('picTodayCtrl', function($scope, $http) {
	$scope.getPicToday = function() {
		let random = $http.get(`https://api.unsplash.com/photos/random?page=1&per_page=1?&client_id=79812888d3772512ecf89e9930213cf5a38fe9bf0b65fe446b409bd90c26d2ec`)
		.then(function(json) {
			$scope.pictoday = json;
			console.log($scope.pictoday);
			let picArtist = document.getElementById('pic-artist');
			let trendingWrapper = document.getElementById('trending-wrapper');
			picArtist.setAttribute("src", `${$scope.pictoday.data.user.profile_image.medium}`);
			trendingWrapper.setAttribute("src", `${$scope.pictoday.data.urls.regular}`);

		})
	}
});;


